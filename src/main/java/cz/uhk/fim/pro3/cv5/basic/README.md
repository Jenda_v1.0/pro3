# MVC návrhový vzor

V tomto balíčku je uvedena (_pouze pro demonstrační účely v rámci předmětu PRO3_) jednoduchá (_basic_) ukázka návrhového
vzoru MVC (_jednotlivých vrstev / komponent_).

V návrhovém vzoru MVC bývá oddělena aplikační (_business_) logika od uživatelského rozhraní.

![](MvcPattern.png)

## Controller

* _Řídí tok aplikace_.
* Drží **Model** a **View** oddělené.
* Reaguje na události (_od uživatele_) a zajišťuje změny v _Modelu_ a _View_.
* Příklad ve třídě `src/main/java/cz/uhk/fim/pro3/cv5/basic/MvcDemo.java`
* **Princip**
    * Přijme požadavek (_nejčastěji z GUI_)
    * Dotáže se na **Model** vrstvu za účelem shromáždění dat (_dle potřeby aktualizace údajů, například profilu
      uživatele_).
    * Kontrolér shromážděné údaje předá **View** vrstvě, kde se data zobrazí a (_nejčastěji_) vrátí výsledek
      klientovi (_zobrazení dat v prohlížeči_).
    * _Viz obrázek výše._

## Model

* Nejčastěji se jedná o _reprezentaci informace_.
    * Může se jednat o databázový objekt nebo _věci_ s tím související (_například výčet či jiné seznamy hodnot či
      konfigurace_).
* Může se jednat i o business logiku potřebnou pro aktualizace dat v modelu nebo kontroléru.
    * Například business logika pro editaci existujícího záznamu v databázi a následné zobrazení změn u klienta v
      prohlížeči.
        * A s tím potřebné věci (_databázová operace / dotaz, validace vstupních údajů, obsloužení chyb a výjimek,
          dotazy na jiné služby, servlety, ..._).

## View

* Vizualizace dat, která jsou obsažena v modelu.
* V této ukázce se jedná pouze o výpis příslušného objektu (_dat_) do konzole, ale v praxi se jedná o soubory
  zobrazující potřebný obsah klientovi v prohlížeči (_JSP, JSP, HTML, ... stránky_).

## Potenciální zdroje a další informace

* https://www.geeksforgeeks.org/mvc-design-pattern/
* https://medium.com/edureka/mvc-architecture-in-java-a85952ae2684
* https://www.javatpoint.com/mvc-architecture-in-java
* https://www.edureka.co/blog/mvc-architecture-in-java/
* https://www.tutorialspoint.com/design_pattern/mvc_pattern.htm
