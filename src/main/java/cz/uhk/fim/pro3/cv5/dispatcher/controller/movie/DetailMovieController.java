package cz.uhk.fim.pro3.cv5.dispatcher.controller.movie;

import cz.uhk.fim.pro3.cv5.dispatcher.controller.Controller;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.suport.MovieSupport;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Movie;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.storage.MovieStorage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Get (/ find) a {@link Movie} based on a given id from a collection (database simulation). <br/> Detailed data
 * (profile) of the respective {@link Movie} will be displayed on the returned page. <br/> If the {@link Movie} is not
 * found, the user will be informed on the displayed (returned) page (found by the 'found' attribute).
 */

public class DetailMovieController implements Controller {

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter(MovieSupport.ATTR_MOVIE_ID));

        boolean found = true;
        Movie movie = MovieStorage.getMovie(id);
        if (movie == null) {
            found = false;
        }

        request.setAttribute(MovieSupport.ATTR_MOVIE_FOUND, found);
        request.setAttribute(MovieSupport.ATTR_MOVIE, movie);
        return MovieSupport.PAGE_DETAIL_MOVIE;
    }

}
