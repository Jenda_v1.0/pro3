package cz.uhk.fim.pro3.cv5.dispatcher.controller.movie;

import cz.uhk.fim.pro3.cv5.dispatcher.controller.Controller;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.suport.MovieSupport;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Movie;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.storage.MovieStorage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Delete a {@link Movie} with the specified id from the collection (database simulation). <br/> Based on the provided
 * id, the {@link Movie} in the collection will be searched, if it is found, it will be deleted. <br/> If the {@link
 * Movie} is not found based on the entered id, the user will be informed of the relevant error according to the
 * relevant attribute ('deleted').
 */

public class DeleteMovieController implements Controller {

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        boolean deleted = false;

        Integer id = Integer.parseInt(request.getParameter(MovieSupport.ATTR_MOVIE_ID));
        Movie movie = MovieStorage.getMovie(id);

        if (movie != null) {
            deleted = MovieStorage.deleteMovie(id);
            request.setAttribute(MovieSupport.ATTR_MOVIE, movie);
        }

        request.setAttribute(MovieSupport.ATTR_MOVIE_DELETED, deleted);
        return "/page/exercises/cv5/view/movie/deleted-movie.jsp";
    }

}
