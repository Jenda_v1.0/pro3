package cz.uhk.fim.pro3.cv5.basic.controller;

import cz.uhk.fim.pro3.cv5.basic.model.Student;
import cz.uhk.fim.pro3.cv5.basic.view.StudentView;

/**
 * Demonstration (/ simulation) of the controller in MVC architecture. <br/>
 * <i>The principle is that it reads (/ processes) the necessary data from the Model layer, which it passes to the View
 * layer and thus returns the result.</i>
 */

public class StudentController {

    private final Student model;
    private final StudentView view;

    public StudentController(Student model, StudentView view) {
        this.model = model;
        this.view = view;
    }

    public void updateId(Integer id) {
        model.setId(id);
    }

    public Integer getId() {
        return model.getId();
    }

    public void updateStudentId(String studentId) {
        model.setStudentId(studentId);
    }

    public String getStudentId() {
        return model.getStudentId();
    }

    public void updateFirstName(String firstName) {
        model.setFirstName(firstName);
    }

    public String getFirstName() {
        return model.getFirstName();
    }

    public void updateLastName(String lastName) {
        model.setLastName(lastName);
    }

    public String getLastName() {
        return model.getLastName();
    }

    public void updateEmail(String email) {
        model.setEmail(email);
    }

    public String getEmail() {
        return model.getEmail();
    }

    public void updateView() {
        view.printStudent(model);
    }

}
