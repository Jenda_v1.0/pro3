package cz.uhk.fim.pro3.cv5.dispatcher.model.movie;

import cz.uhk.fim.pro3.cv5.dispatcher.model.person.Person;

import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

/**
 * Database entity simulation for working with movies in the application. <br/> The object could also contain, for
 * example, attributes for the people who 'managed' the music, the camera, the script and the actors in the film. <br/>
 * <i>But for these demonstration purposes and to facilitate working with forms (especially creating a list of actors),
 * this data has been omitted.</i>
 */

public class Movie {

    private Integer id;
    private String title;
    private Person director;
    private List<Genre> genres;
    private Date releaseDate;
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Person getDirector() {
        return director;
    }

    public void setDirector(Person director) {
        this.director = director;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Movie.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("title='" + title + "'")
                .add("director=" + director)
                .add("genres=" + genres)
                .add("releaseDate=" + releaseDate)
                .add("description='" + description + "'")
                .toString();
    }

}
