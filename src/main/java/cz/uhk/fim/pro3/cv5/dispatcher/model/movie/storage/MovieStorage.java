package cz.uhk.fim.pro3.cv5.dispatcher.model.movie.storage;

import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Movie;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Database simulation for storing movies.
 */

public class MovieStorage {

    private static final List<Movie> movies = new ArrayList<>();

    public static boolean addMovie(Movie movie) {
        return movies.add(movie);
    }

    public static Movie getMovie(Integer id) {
        return movies.stream()
                     .filter(movie -> movie.getId().equals(id))
                     .findFirst()
                     .orElse(null);
    }

    public static List<Movie> filterMovie(String title) {
        return movies.stream()
                     .filter(movie -> movie.getTitle().toUpperCase().contains(title.toUpperCase()))
                     .collect(Collectors.toList());
    }

    public static boolean updateMovie(Movie movie) {
        int index = findIndexOfMovie(movie.getId());

        if (index > -1) {
            movies.set(index, movie);
            return true;
        }
        return false;
    }

    private static int findIndexOfMovie(Integer id) {
        return IntStream.range(0, movies.size())
                        .filter(i -> movies.get(i).getId().equals(id))
                        .findFirst()
                        .orElse(-1);
    }

    public static boolean deleteMovie(Integer id) {
        return movies.removeIf(movie -> movie.getId().equals(id));
    }

    public static List<Movie> getMovies() {
        return movies;
    }

}
