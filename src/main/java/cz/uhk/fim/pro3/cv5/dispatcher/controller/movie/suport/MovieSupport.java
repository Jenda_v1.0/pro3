package cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.suport;

import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Genre;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Movie;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Common attributes and methods for working with controllers in connection with {@link Movie} (Controller layer).
 */

public class MovieSupport {

    public static final String PAGE_DETAIL_MOVIE = "/page/exercises/cv5/view/movie/detail-movie.jsp";
    public static final String PAGE_ERROR_MOVIE = "/page/exercises/cv5/view/movie/movie-error.jsp";

    public static final String ATTR_MOVIE = "movie";
    public static final String ATTR_MOVIE_ID = "id";
    public static final String ATTR_MOVIE_FOUND = "found";
    public static final String ATTR_MOVIE_DELETED = "deleted";
    public static final String ATTR_MOVIE_ERROR = "error";

    private static final Random random = new Random();

    /**
     * Get data from the HTTP request parameter named in the parameter method 'parameter'. <br/> The parameter value
     * will be converted to UTF-8 encoding.
     *
     * @param request
     *         object that contains the request the client has made of the servlet (to obtain the parameter)
     * @param parameter
     *         the name of the parameter (/ attribute) whose value will be retrieved, converted to the specified
     *         encoding, and returned
     *
     * @return the value of the relevant parameter in the required encoding
     */
    public static String getParameterInUtf8(HttpServletRequest request, String parameter) {
        return new String(request.getParameter(parameter).getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }

    /**
     * Getting an integer from the HTTP request and converting it to the corresponding data type (if the parameter is
     * specified).
     *
     * @param request
     *         object that contains the request the client has made of the servlet (to obtain the parameter)
     * @param parameter
     *         the name of the parameter in the HTTP request whose value is to be retrieved
     *
     * @return integer obtained from the HTTP request parameter, otherwise zero (default value)
     */
    public static Integer getNumber(HttpServletRequest request, String parameter) {
        String txtNumber = request.getParameter(parameter);
        if (txtNumber != null && txtNumber.replaceAll("\\s", "").length() > 0 && txtNumber.matches("^-?\\d+$")) {
            return Integer.parseInt(txtNumber);
        }
        return 0;
    }

    /**
     * Getting the {@link Date} from the HTTP request parameter. <br/>
     * <i>If the appropriate value is specified in the HTTP request parameter, it will be converted to the {@link Date}
     * data type and returned.</i> <br/>
     * <i>If the value is not specified in the HTTP request parameter, null is returned.</i>
     *
     * @param request
     *         object that contains the request the client has made of the servlet (to obtain the parameter)
     * @param parameter
     *         the name of the parameter in the HTTP request whose value is to be retrieved
     *
     * @return date obtained from the HTTP request, otherwise null (as well as when an exception occurs when trying to
     * parse an invalid value)
     */
    public static Date getDateFromParameter(HttpServletRequest request, String parameter) {
        try {
            String dateParameter = request.getParameter(parameter);
            if (dateParameter == null || dateParameter.replaceAll("\\s", "").length() == 0) {
                return null;
            }
            return new SimpleDateFormat("yyyy-MM-dd")
                    .parse(dateParameter);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int generateRandomId() {
        return random.nextInt(1000);
    }

    /**
     * Getting a list of {@link Movie} genres from an HTTP request parameter. <br/> A list (array) of relevant values
     * will be obtained from the HTTP request parameter, which will be converted to a list of corresponding enumerated
     * data types and will be returned. <br/> if no parameter is specified in the HTTP request, an empty list will be
     * returned
     *
     * @param request
     *         object that contains the request the client has made of the servlet (to obtain the parameter)
     *
     * @return a list of genres obtained from the HTTP request, otherwise an empty list
     */
    public static List<Genre> getGenresFromParameter(HttpServletRequest request) {
        String[] genres = request.getParameterValues("genres");
        if (genres == null || genres.length == 0) {
            return Collections.emptyList();
        }

        return Stream.of(genres)
                     .map(Genre::valueOf)
                     .collect(Collectors.toList());
    }

}
