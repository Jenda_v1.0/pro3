package cz.uhk.fim.pro3.cv5.dispatcher.controller.movie;

import cz.uhk.fim.pro3.cv5.dispatcher.controller.Controller;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.suport.MovieSupport;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Movie;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.storage.MovieStorage;
import cz.uhk.fim.pro3.cv5.dispatcher.model.person.Person;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Add (/ create) a new {@link Movie} and save it to the collection (database simulation). <br/> The necessary
 * parameters will be obtained from the HTTP request (data from the form), (it would be good to validate them) the
 * corresponding {@link Movie} object will be created from them and it will be added to the collection. <br/> If an
 * error occurs, a link to a page informing the user of the error will be returned. Otherwise, a page showing the detail
 * of the created {@link Movie} will be returned.
 */

public class AddMovieController implements Controller {

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        // Obtaining data (parameters) from the request (data from the form)
        String title = request.getParameter("title");
        String directorFirstName = MovieSupport.getParameterInUtf8(request, "director.firstName");
        String directorLastName = MovieSupport.getParameterInUtf8(request, "director.lastName");
        int directorAge = MovieSupport.getNumber(request, "director.age");
        Date directorBirthday = MovieSupport.getDateFromParameter(request, "director.birthday");
        Date releaseDate = MovieSupport.getDateFromParameter(request, "releaseDate");
        String description = MovieSupport.getParameterInUtf8(request, "description");

        // At this stage, it would be appropriate to add the validation of the obtained data (presence of mandatory attribute values, check the syntax of the obtained values, ...), but for demonstration purposes this part is omitted (this is not the content of this exercise)

        Person director = new Person();
        director.setId(MovieSupport.generateRandomId());
        director.setFirstName(directorFirstName);
        director.setLastName(directorLastName);
        director.setAge(directorAge);
        director.setBirthday(directorBirthday);

        Movie movie = new Movie();
        movie.setId(MovieSupport.generateRandomId());
        movie.setTitle(title);
        movie.setDirector(director);
        movie.setGenres(MovieSupport.getGenresFromParameter(request));
        movie.setReleaseDate(releaseDate);
        movie.setDescription(description);

        // Save (/ add) a new movie to the database
        boolean added = MovieStorage.addMovie(movie);
        // If the movie fails to add (/ 'create'), information about it will be listed (on the 'error' page)
        if (!added) {
            String errorMessage = "Film '" + title + "' se nepodařilo přidat";
            request.setAttribute(MovieSupport.ATTR_MOVIE_ERROR, errorMessage);
            return MovieSupport.PAGE_ERROR_MOVIE;
        }

        request.setAttribute(MovieSupport.ATTR_MOVIE_FOUND, true);
        request.setAttribute(MovieSupport.ATTR_MOVIE, movie);
        return MovieSupport.PAGE_DETAIL_MOVIE;
    }

}
