package cz.uhk.fim.pro3.cv5.dispatcher.controller.movie;

import cz.uhk.fim.pro3.cv5.dispatcher.controller.Controller;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.suport.MovieSupport;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Movie;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.storage.MovieStorage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Find and load (from collection - database simulation) {@link Movie} based on passed id. This movie will be displayed
 * on the movie data editing page. <br/> Based on the provided id, a {@link Movie} will be searched, the data of which
 * will be inserted into the text field in form on the relevant (returned) page, where the user will be able to edit
 * them. <br/> If the {@link Movie} is not found based on the passed id, information about it will be displayed on the
 * page (identified by the 'found' attribute).
 */

public class GetMovieForUpdateMovieController implements Controller {

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter(MovieSupport.ATTR_MOVIE_ID));

        boolean found = true;
        Movie movie = MovieStorage.getMovie(id);
        if (movie == null) {
            found = false;
        }

        request.setAttribute(MovieSupport.ATTR_MOVIE_FOUND, found);
        request.setAttribute(MovieSupport.ATTR_MOVIE, movie);
        return "/page/exercises/cv5/view/movie/update-movie.jsp";
    }

}
