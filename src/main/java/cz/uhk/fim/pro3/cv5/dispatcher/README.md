# Centrální (_dispečer_) servlet

V tomto balíčku je uvedena (_pouze pro demonstrační účely v rámci předmětu PRO3_) jednoduchá ukázka (_principu_) **
DispatcherServlet-u** (_dispečeru_) embedovaného ve Spring-u.

**Slouží** pro přijímání HTTP požadavků, které směruje na konkrétní kontrolér a následné přeposílání HTTP odpovědí
klientům (*zobrazení údajů v prohlížeči*).

**Kontrolér** přijme požadavek a zavolá konkrétní službu pro vykonání business logiky (_operace s daty_), která _
nastaví_ potřená data v modelu a vrátí název (_odkaz_) na (_webovou_) stránku (_View_) do dispečeru.

**Dispečer** načte požadovanou stránku (_soubor_) definovanou ve View (_pro konkrétní požadavek_).

**Dispečer** (_po dokončení / načtení View_) předá data modelu pohledu, který je nakonec vykreslen v prohlížeči klienta.

## Potenciální zdroje a další informace

* Dispečer
    * https://www.baeldung.com/spring-dispatcherservlet
    * https://www.tutorialspoint.com/spring/spring_web_mvc_framework.htm
    * https://howtodoinjava.com/spring5/webmvc/spring-dispatcherservlet-tutorial/
* Mapování (_vzory URL adres dispečeru_)
    * https://cloud.google.com/appengine/docs/flexible/java/configuring-the-web-xml-deployment-descriptor
