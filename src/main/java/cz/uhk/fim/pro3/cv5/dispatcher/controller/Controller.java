package cz.uhk.fim.pro3.cv5.dispatcher.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The method that each controller will implement will contain specific business logic to handle the request. <br/>
 * <i>It is also necessary that all controllers can be 'mapped' in the {@link cz.uhk.fim.pro3.cv5.dispatcher.dispatcher.DispatcherServlet}.</i>
 */

public interface Controller {

    /**
     * Serving the request by querying the Model layer for data, performing an operation with it as needed, and passing
     * the result to the View layer for display to the user.
     *
     * @param request
     *         object that contains the request the client has made of the servlet
     * @param response
     *         object that contains the response the servlet sends to the client
     *
     * @return link (/ path) to the JSP page for display to the user (display of required data)
     */
    String handleRequest(HttpServletRequest request, HttpServletResponse response);

}
