package cz.uhk.fim.pro3.cv5.basic.view;

import cz.uhk.fim.pro3.cv5.basic.model.Student;

/**
 * Sample (/ simulation) View layer in MVC architecture. <br/>
 * <i>Used to display the result to the user. In this case, we only print the object to the console.</i>
 */

public class StudentView {

    public void printStudent(Student student) {
        System.out.println(student);
    }

}
