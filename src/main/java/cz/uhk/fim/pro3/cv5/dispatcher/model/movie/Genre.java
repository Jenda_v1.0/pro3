package cz.uhk.fim.pro3.cv5.dispatcher.model.movie;

/**
 * List of action {@link Movie} genres.
 */

public enum Genre {

    ACTION,
    COMEDY,
    DRAMA,
    FANTASY,
    HORROR,
    MYSTERY,
    ROMANCE,
    THRILLER,
    WESTERN,
    BIBLIOGRAPHY,
    DOCUMENT,
    HISTORY

}
