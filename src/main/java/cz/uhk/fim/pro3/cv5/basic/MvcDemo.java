package cz.uhk.fim.pro3.cv5.basic;

import cz.uhk.fim.pro3.cv5.basic.controller.StudentController;
import cz.uhk.fim.pro3.cv5.basic.model.Student;
import cz.uhk.fim.pro3.cv5.basic.view.StudentView;

import java.util.Random;
import java.util.UUID;

/**
 * Example of mutual communication of individual components (/ layers) in MVC architecture.
 */

public class MvcDemo {

    public static void main(String[] args) {
        // Loading a student from the database according to his id (it can be an operation in the model)
        Student student = loadStudentFromDatabase(new Random().nextInt(100));

        // Creating a controller (with View) to update the view (student listing to the console)
        StudentController controller = new StudentController(student, new StudentView());

        controller.updateView();

        // Update data in the model
        controller.updateId(new Random().nextInt(100));
        controller.updateStudentId(UUID.randomUUID().toString());
        controller.updateFirstName("Ned");
        controller.updateLastName("Flanders");
        controller.updateEmail("ned.flanders@uhk.cz");

        controller.updateView();
    }

    private static Student loadStudentFromDatabase(Integer id) {
        Student student = new Student();
        student.setId(id);
        student.setStudentId(UUID.randomUUID().toString());
        student.setFirstName("March");
        student.setLastName("Simpson");
        student.setEmail("march.simpson@uhk.cz");
        return student;
    }

}
