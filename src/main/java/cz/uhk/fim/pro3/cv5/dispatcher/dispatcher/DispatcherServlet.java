package cz.uhk.fim.pro3.cv5.dispatcher.dispatcher;

import cz.uhk.fim.pro3.cv5.dispatcher.controller.Controller;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.AddMovieController;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.DeleteMovieController;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.DetailMovieController;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.GetMovieForUpdateMovieController;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.ListMovieController;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.UpdateMovieController;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.person.PersonController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * A 'central servlet' that handles all requests that have been sent to an address that matches a specific pattern
 * (/<anything>.dispatch). <br/> It is possible to write a 'central servlet' for certain (CRUD) operations with a
 * certain entity. <br/> For example, for operations with users, write the central servlet '*.person', for operations
 * with movies '*.movie', etc. Alternatively, the names can be adapted as needed (own pattern).
 */

@WebServlet(name = "DispatcherServlet", urlPatterns = "*.dispatch")
public class DispatcherServlet extends HttpServlet {

    /**
     * The name of the page to display if the required controller is not found to service the request. <br/>
     * <i>Error page containing information about 'some' error. In this case, an unknown URL.</i>
     */
    private static final String ERROR_PAGE = "page/exercises/cv5/error.jsp";

    /**
     * A list of all 'known' addresses to which the request can be sent and 'this' DispatcherServlet will be able to
     * handle them. <br/>
     * <i>That is, known addresses for DispatcherServlet.</i>
     * <br/>
     * <i>Based on the address found in this map, a controller will be selected to handle the request.</i>
     */
    private static final Map<String, Controller> urlMapping = new HashMap<>();

    private static void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Search for a controller to handle the request based on the address to which the request was sent
        Controller controller = urlMapping.get(request.getServletPath());

        // If the controller has been found, its business logic will be executed and the result will be returned (the user will be redirected to a specific page)
        if (controller != null) {
            String view = controller.handleRequest(request, response);
            request.getRequestDispatcher(view)
                   .forward(request, response);
            return;
        }

        // Default handling of the request if it was sent to an unknown address
        request.setAttribute("url", request.getServletPath());
        request.getRequestDispatcher(ERROR_PAGE)
               .forward(request, response);
    }

    @Override
    public void init() {
        urlMapping.put("/person.dispatch", new PersonController());
        urlMapping.put("/add-movie.dispatch", new AddMovieController());
        urlMapping.put("/get-movie-for-update-movie.dispatch", new GetMovieForUpdateMovieController());
        urlMapping.put("/update-movie.dispatch", new UpdateMovieController());
        urlMapping.put("/detail-movie.dispatch", new DetailMovieController());
        urlMapping.put("/delete-movie.dispatch", new DeleteMovieController());
        urlMapping.put("/list-movie.dispatch", new ListMovieController());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
