package cz.uhk.fim.pro3.cv5.dispatcher.controller.movie;

import cz.uhk.fim.pro3.cv5.dispatcher.controller.Controller;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Movie;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.storage.MovieStorage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * View a list of all or specific movies in a collection (database simulation). <br/> If the appropriate attribute is
 * passed in the HTTP request parameter, only certain / filtered movies that meet the relevant criteria will be
 * returned. <br/>
 * <i>Movies will be filtered based on the match of the {@link Movie} title (movie title must contain the appropriate
 * text).</i> <br/> If the above attribute is not passed in the HTTP request parameter, all movies in the collection
 * will be returned.
 */

public class ListMovieController implements Controller {

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        List<Movie> movies;

        // If a title (parameter) has been passed, only movies containing the entered text will be filtered, otherwise all movies will be loaded (returned)
        // (It would also be useful to add validation of at least allowed characters)
        String title = request.getParameter("title");
        if (title != null && title.replaceAll("\\s", "").length() > 0) {
            title = title.trim();
            movies = MovieStorage.filterMovie(title);
        } else {
            movies = MovieStorage.getMovies();
        }

        request.setAttribute("movies", movies);
        return "/page/exercises/cv5/view/movie/list-movie.jsp";
    }

}
