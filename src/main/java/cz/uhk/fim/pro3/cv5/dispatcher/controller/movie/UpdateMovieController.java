package cz.uhk.fim.pro3.cv5.dispatcher.controller.movie;

import cz.uhk.fim.pro3.cv5.dispatcher.controller.Controller;
import cz.uhk.fim.pro3.cv5.dispatcher.controller.movie.suport.MovieSupport;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.Movie;
import cz.uhk.fim.pro3.cv5.dispatcher.model.movie.storage.MovieStorage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Update data for an existing {@link Movie} in the collection (database simulation). <br/> Based on the {@link Movie}
 * id obtained from the HTTP request parameter, an existing movie will be searched, the data of which will be modified
 * based on the modified data from the form (parameters in the HTTP request). <br/>
 * <i>It would be good to fine-tune how the data should be updated (edit only the values listed in the form or those
 * that do not match, how to set the data that should not be edited, etc.). This is for 'simple' demonstration purposes
 * only.</i> <br/> If the movie is not found in the collection based on the passed id, or an error occurs while editing
 * it, this information will be displayed on the returned page (either the error page or the detail page of the edited
 * movie).
 */

public class UpdateMovieController implements Controller {

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        // Obtaining data (parameters) from the request (data from the form)
        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String directorFirstName = MovieSupport.getParameterInUtf8(request, "director.firstName");
        String directorLastName = MovieSupport.getParameterInUtf8(request, "director.lastName");
        int directorAge = MovieSupport.getNumber(request, "director.age");
        Date directorBirthday = MovieSupport.getDateFromParameter(request, "director.birthday");
        Date releaseDate = MovieSupport.getDateFromParameter(request, "releaseDate");
        String description = MovieSupport.getParameterInUtf8(request, "description");

        // At this stage, it would be appropriate to add the validation of the obtained data (presence of mandatory attribute values, check the syntax of the obtained values, ...), but for demonstration purposes this part is omitted (this is not the content of this exercise)

        boolean found = true;
        Movie movie = MovieStorage.getMovie(id);

        if (movie != null) {
            movie.setTitle(title);
            movie.getDirector().setFirstName(directorFirstName);
            movie.getDirector().setLastName(directorLastName);
            movie.getDirector().setAge(directorAge);
            movie.getDirector().setBirthday(directorBirthday);
            movie.setGenres(MovieSupport.getGenresFromParameter(request));
            movie.setReleaseDate(releaseDate);
            movie.setDescription(description);

            // If the movie fails to update, information about it will be listed (on the 'error' page)
            boolean updated = MovieStorage.updateMovie(movie);
            if (!updated) {
                String errorMessage = "Ve filmu '" + movie.getTitle() + "' se nepodařilo uložit změny.";
                request.setAttribute(MovieSupport.ATTR_MOVIE_ERROR, errorMessage);
                return MovieSupport.PAGE_ERROR_MOVIE;
            }
        } else {
            found = false;
        }

        request.setAttribute(MovieSupport.ATTR_MOVIE_FOUND, found);
        request.setAttribute(MovieSupport.ATTR_MOVIE, movie);
        return MovieSupport.PAGE_DETAIL_MOVIE;
    }

}
