package cz.uhk.fim.pro3.cv5.dispatcher.controller.person;

import cz.uhk.fim.pro3.cv5.dispatcher.controller.Controller;
import cz.uhk.fim.pro3.cv5.dispatcher.model.person.Person;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Random;

/**
 * A sample controller within the MVC design pattern that 'collects' data about the requested user ({@link Person}),
 * inserts the data into the View layer (HTTP request attribute) and returns the result (display page).
 */

public class PersonController implements Controller {

    @Override
    public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
        // Sample Model (layer) for retrieving a person from the database
        Person person = new Person();
        person.setId(new Random().nextInt(100));
        person.setFirstName("Homer");
        person.setLastName("Simpson");
        person.setAge(30);

        ZonedDateTime birthday = ZonedDateTime.now().minusYears(person.getAge());
        person.setBirthday(Date.from(birthday.toInstant()));

        // Passing data to the MVC View layer
        request.setAttribute("person", person);

        // Return result (link to the relevant information page displayed to the user)
        return "/page/exercises/cv5/view/person/person.jsp";
    }

}
