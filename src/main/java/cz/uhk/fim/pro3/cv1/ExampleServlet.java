package cz.uhk.fim.pro3.cv1;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Class {@link HttpServlet} provides an abstract class to be subclassed to create an HTTP servlet suitable for a Web
 * site. A subclass of HttpServlet must override at least one method, usually one of these: <br/> - doGet, if the
 * servlet supports HTTP GET requests <br/> - doPost, for HTTP POST requests <br/> - doPut, for HTTP PUT requests <br/>
 * - doDelete, for HTTP DELETE requests <br/> - init and destroy, to manage resources that are held for the life of the
 * servlet <br/> - getServletInfo, which the servlet uses to provide information about itself
 */
@WebServlet(name = "ExampleServlet", value = "/example-servlet")
public class ExampleServlet extends HttpServlet {

    /**
     * Called by the server (via the service method) to allow a servlet to handle a GET request. <br/> Overriding this
     * method to support a GET request also automatically supports an HTTP HEAD request. A HEAD request is a GET request
     * that returns no body in the response, only the request header fields. <br/> <br/> Every time a client makes an
     * HTTP GET request to this servlet, this method is processed. The servlet container injects two objects: <br/> -
     * HttpServletRequest: this object wraps all HTTP request headers so we can access GET/POST parameters as well as
     * other HTTP headers sent from the client, via the methods getParameter() and getHeader(), respectively. <br/> -
     * HttpServletResponse: we use this object to deal with HTTP response such as setting HTTP response headers and
     * sending HTML content back to the client. In this servlet’s doGet() method, we send a simple HTML code which
     * conveys a message “Hello, I am a Java servlet!” to the client.
     *
     * @param request
     *         object that contains the request the client has made of the servlet
     * @param response
     *         object that contains the response the servlet sends to the client
     *
     * @throws ServletException
     *         if the request for the GET could not be handled
     * @throws IOException
     *         if an input or output error is detected when the servlet handles the GET request
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("Example Servlet");
        out.println("</title></head><body>");

        // Add a page body
        out.println("<h1>" + "Hello World z Example Servlet" + "</h1>");
        out.println("<div>Další popis</div>");
        out.println("<a href=\"/page/exercises/cv1/cv1.html\">Cvičení 1</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

    /**
     * Called by the server (via the service method) to allow a servlet to handle a POST request. The HTTP POST method
     * allows the client to send data of unlimited length to the Web server a single time and is useful when posting
     * information such as credit card numbers. <br/> When overriding this method, read the request data, write the
     * response headers, get the response's writer or output stream object, and finally, write the response data. It's
     * best to include content type and encoding. When using a PrintWriter object to return the response, set the
     * content type before accessing the PrintWriter object. <br/> <br/> This method is invoked whenever a client makes
     * an HTTP POST request. Similar to the doGet() method, it also takes an HttpServletRequest object and
     * HttpServletResponse object as arguments.
     *
     * @param request
     *         object that contains the request the client has made of the servlet
     * @param response
     *         object that contains the response the servlet sends to the client
     *
     * @throws ServletException
     *         if the request for the POST could not be handled
     * @throws IOException
     *         if an input or output error is detected when the servlet handles the request
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Handling the POST request in the same way as GET
        doGet(request, response);
    }

    /**
     * The servlet container will invoke this method when it needs to remove the servlet, as the containing application
     * is being stopped or the server is shutting down.
     */
    @Override
    public void destroy() {
        System.out.println("Servlet is being destroyed");

        // Is invoked only once and indicates that servlet is being destroyed

        super.destroy();
    }

    @Override
    public String getServletInfo() {
        // Returns information about servlet such as writer, copyright, version etc.
        //return "A simple description of what this Servlet is for";
        return super.getServletInfo();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("Servlet is being initialized");

        // Initializes the servlet, it is the life cycle method of servlet and invoked by the web container only once

        super.init(config);
    }

    /**
     * The servlet container will invoke this method when the servlet is first accessed by the client. It’s common to
     * place initialization code here. This method is called only once in the servlet’s life.
     *
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        System.out.println("Servlet initialized");
        super.init();
    }

}
