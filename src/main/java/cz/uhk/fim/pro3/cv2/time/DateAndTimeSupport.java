package cz.uhk.fim.pro3.cv2.time;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Format the current date and time to the desired form.
 */
public class DateAndTimeSupport {

    public static String getTime() {
        return new SimpleDateFormat("HH:mm:ss dd.MM.yyyy")
                .format(new Date());
    }

}
