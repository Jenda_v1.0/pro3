package cz.uhk.fim.pro3.cv2;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Display HTTP request headers.
 */

@WebServlet(name = "HeaderServlet", value = "/header-servlet")
public class HeaderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("Hlavička");
        out.println("</title></head><body>");

        // Page body
        out.println("<h1>" + "Hlavička" + "</h1>");

        // Insert headers in the page that will be displayed to the user
        out.println("<ul>");
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headerValue = request.getHeader(headerName);
            out.println("<li><b>" + headerName + " = " + headerValue + "</b></li>");
        }
        out.println("</ul>");

        out.println("<br/>");
        out.println("<a href=\"/page/exercises/cv2/cv2.html\">Cvičení 2</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

}
