package cz.uhk.fim.pro3.cv2.calculator;

/**
 * Based on the obtained operation, a certain arithmetic operation with two numbers will be performed.
 */
public class CalculatorSupport {

    public static Integer compute(String txtNumber1, String txtNumber2, String operation) {
        int number1 = Integer.parseInt(txtNumber1);
        int number2 = Integer.parseInt(txtNumber2);

        if ("addition".equals(operation)) {
            return number1 + number2;
        }
        if ("subtraction".equals(operation)) {
            return number1 - number2;
        }
        if ("division".equals(operation)) {
            return number1 / number2;
        }

        // Default operation (multiplication)
        return number1 * number2;
    }

}
