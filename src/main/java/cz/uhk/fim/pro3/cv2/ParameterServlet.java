package cz.uhk.fim.pro3.cv2;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet(name = "ParameterServlet", value = "/parameter-servlet")
public class ParameterServlet extends HttpServlet {

    private static void printParameters(PrintWriter out, HttpServletRequest request) {
        out.println("<h1>" + "Parametry" + "</h1>");

        // Insert the obtained parameters into the page for display
        out.println("<ul>");
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String parameterName = parameterNames.nextElement();
            String parameterValue = request.getParameter(parameterName);
            out.println("<li><b>" + parameterName + " = " + parameterValue + "</b></li>");
        }
        out.println("</ul>");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("GET - Parametry");
        out.println("</title></head><body>");

        // Page body
        out.println("<h1>" + "GET - Parametry" + "</h1>");

        printParameters(out, request);

        out.println("<br/>");
        out.println("<a href=\"/page/exercises/cv2/cv2.html\">Cvičení 2</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("POST - Parametry");
        out.println("</title></head><body>");

        // Page body
        out.println("<h1>" + "POST - Parametry" + "</h1>");

        printParameters(out, request);

        out.println("<br/>");
        out.println("<a href=\"/page/exercises/cv2/cv2.html\">Cvičení 2</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

}
