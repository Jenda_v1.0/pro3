package cz.uhk.fim.pro3.cv2;

import cz.uhk.fim.pro3.cv2.time.DateAndTimeSupport;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Display the current date and time.
 */

@WebServlet(name = "DateAndTimeServlet", value = "/date-and-time-servlet")
public class DateAndTimeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setIntHeader("Refresh", 1);

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("Datum a čas");
        out.println("</title></head><body>");

        // Page body
        out.println("<h1>" + "Datum a čas" + "</h1>");

        out.println("<div>Aktuálně je <b>" + DateAndTimeSupport.getTime() + "</b></div>");

        out.println("<br/>");
        out.println("<a href=\"/page/exercises/cv2/cv2.html\">Cvičení 2</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

}
