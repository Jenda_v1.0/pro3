package cz.uhk.fim.pro3.cv2;

import cz.uhk.fim.pro3.cv2.calculator.CalculatorSupport;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Simple calculator. <br/> The servlet obtains two numbers from the request and the arithmetic operation it performs on
 * the numbers. <br/> The result is displayed on the returned page.
 */
@WebServlet(name = "CalculatorServlet", value = "/calculator-servlet")
public class CalculatorServlet extends HttpServlet {

    private static String getCharForOperation(String operation) {
        if ("addition".equals(operation)) {
            return "+";
        }
        if ("subtraction".equals(operation)) {
            return "-";
        }
        if ("division".equals(operation)) {
            return "/";
        }
        if ("multiplication".equals(operation)) {
            return "*";
        }
        return "UNKNOWN_OPERATION";
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("Výsledek");
        out.println("</title></head><body>");

        // Page body
        out.println("<h1>" + "Výsledek" + "</h1>");

        // Obtaining parameters
        String txtNumber1 = request.getParameter("number1");
        String txtNumber2 = request.getParameter("number2");
        String operation = request.getParameter("operation");

        // Result display
        Integer result = CalculatorSupport.compute(txtNumber1, txtNumber2, operation);
        out.println("<h3>" + txtNumber1 + " " + getCharForOperation(operation) + " " + txtNumber2 + " = " + result + "</h3>");

        out.println("<br/>");
        out.println("<a href=\"/page/exercises/cv2/cv2.html\">Cvičení 2</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

}
