package cz.uhk.fim.pro3.cv3;

import cz.uhk.fim.pro3.cv3.user.User;
import cz.uhk.fim.pro3.cv3.user.UserStorage;
import cz.uhk.fim.pro3.cv3.user.UserSupport;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * User registration example. <br/>
 * <i>Example of validation of input data, regular expressions and reaction to missing mandatory data, or incorrect
 * syntax of the given data.</i>
 */

@WebServlet(name = "RegistrationServlet", value = "/registration-servlet")
public class RegistrationServlet extends HttpServlet {

    private static final String REG_EX_NAME = "^[A-ZÁ-Ž][a-zA-ZÁ-Ž]{0,50}$";
    private static final String REG_EX_AGE = "^[1-9]\\d{0,2}$";

    private static void printBodyEnd(PrintWriter out) {
        out.println("<br/>");
        out.println("<a href=\"/page/exercises/cv3/cv3.jsp\">Cvičení 3</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

    /**
     * Get data from the HTTP request parameter named in the parameter method 'parameter'. <br/> The parameter value
     * will be converted to UTF-8 encoding.
     *
     * @param request
     *         object that contains the request the client has made of the servlet (to obtain the parameter)
     * @param parameter
     *         the name of the parameter (/ attribute) whose value will be retrieved, converted to the specified
     *         encoding, and returned
     *
     * @return the value of the relevant parameter in the required encoding
     */
    private static String getParameterInUtf8(HttpServletRequest request, String parameter) {
        return new String(request.getParameter(parameter).getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("Registrace");
        out.println("</title></head><body>");

        out.println("<h1>" + "Registrace" + "</h1>");

        // Getting parameters from HTTP request
        String firstName = getParameterInUtf8(request, "firstName");
        String lastName = getParameterInUtf8(request, "lastName");
        String age = request.getParameter("age");
        String email = request.getParameter("email");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");

        // Validation of input data:

        // Validation violation messages
        List<String> validationViolations = new ArrayList<>();

        // Check if the first name is listed
        if (firstName == null || firstName.length() == 0) {
            validationViolations.add("Křestní jméno není uvedeno.");
        }
        // Check that the first name matches the required syntax
        else if (!firstName.matches(REG_EX_NAME)) {
            validationViolations.add("Křestní jméno ('" + firstName + "') musí začínat velkým písmenem a může obsahovat pouze velká a malá písmena s nebo bez diakritiky.");
        }

        // Check if the last name is listed
        if (lastName == null || lastName.length() == 0) {
            validationViolations.add("Příjmení není uvedeno.");
        }
        // Check that the last name matches the required syntax
        else if (!lastName.matches(REG_EX_NAME)) {
            validationViolations.add("Příjmení ('" + lastName + "') musí začínat velkým písmenem a může obsahovat pouze velká a malá písmena s nebo bez diakritiky.");
        }

        // Check if the age matches the required syntax (if specified)
        if (age != null && age.length() > 0 && !age.matches(REG_EX_AGE)) {
            validationViolations.add("Věk ('" + age + "') může být pouze celé číslo větší nebo rovno jedné.");
        }

        // Check if the email is listed
        if (email == null || email.length() == 0) {
            validationViolations.add("Email není uveden.");
        } else if (!UserSupport.validateEmailSyntax(email)) {
            validationViolations.add("Email ('" + email + "') neodpovídá požadované syntaxi (například: 'homer.simpson@uhk.cz') nebo obsahuje nepovolené znaky.");
        }

        // Check if the password1 is listed
        if (password1 == null || password1.length() == 0) {
            validationViolations.add("Heslo 1 není uvedeno.");
        } else if (!UserSupport.validatePasswordSyntax(password1)) {
            validationViolations.add("Heslo 1 ('" + password1 + "') neodpovídá požadované syntaxi. Heslo musí obsahovat 5 až 50 znaků. Musí obsahovat alespoň jedno číslo, jedno velké písmeno, jedno malé písmeno a cizí znak (/*\\-+#%).");
        }

        // Check if the password2 is listed
        if (password2 == null || password2.length() == 0) {
            validationViolations.add("Heslo 2 není uvedeno.");
        } else if (!UserSupport.validatePasswordSyntax(password2)) {
            validationViolations.add("Heslo 2 ('" + password2 + "') neodpovídá požadované syntaxi. Heslo musí obsahovat 5 až 50 znaků. Musí obsahovat alespoň jedno číslo, jedno velké písmeno, jedno malé písmeno a cizí znak (/*\\-+#%).");
        }

        // Check if both passwords are the same
        if (password1 != null && password1.length() > 0 && password2 != null && password2.length() > 0 && !password1.equals(password2)) {
            validationViolations.add("Hesla 1 a 2 jsou různá.");
        }

        // If there is at least one validation violation, we will display information about it
        if (!validationViolations.isEmpty()) {
            out.println("<h2>Porušení validace</h2>");
            out.println("<ul>");
            validationViolations.forEach(validationViolation -> out.println("<li>" + validationViolation + "</li>"));
            out.println("</ul>");

            printBodyEnd(out);
            return;
        }

        // Create a new object (/ user) to be registered
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password1);
        if (age != null && age.length() > 0) {
            // The age will be set only if it is specified and thanks to the syntax check above we know that it is a valid number
            user.setAge(Integer.parseInt(age));
        }

        boolean registerUser = UserStorage.registerUser(user);
        if (!registerUser) {
            out.println("<h2>Uživatele <i>" + user.getFirstName() + " " + user.getLastName() + "</i> se nepodařilo registrovat.</h2>");
            printBodyEnd(out);
            return;
        }

        out.println("<h2>Uživatel <i>" + user.getFirstName() + " " + user.getLastName() + "</i> byl úspěšně registrován.</h2>");

        printBodyEnd(out);
    }

}
