package cz.uhk.fim.pro3.cv3.user;

/**
 * Common methods for servlets for user registration and login.
 */

public class UserSupport {

    /**
     * The name of the attribute in which the information about the logged in user will be stored (in Session).
     */
    public static final String LOGGED_IN_USER_ATTRIBUTE_NAME = "loggedInUser";

    private static final String REG_EX_EMAIL = "^[\\w][\\w.%+\\-]*@[\\w\\-.]+\\.[a-zA-Z]{2,10}$";
    private static final String REG_EX_PASSWORD = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[/*\\-+#%]).{5,50}$";

    public static boolean validateEmailSyntax(String email) {
        return email == null || email.length() <= 0 || email.matches(REG_EX_EMAIL);
    }

    public static boolean validatePasswordSyntax(String password) {
        return password == null || password.length() <= 0 || password.matches(REG_EX_PASSWORD);
    }

}
