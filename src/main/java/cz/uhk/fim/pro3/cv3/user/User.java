package cz.uhk.fim.pro3.cv3.user;

import java.io.Serializable;
import java.util.StringJoiner;

/**
 * Database entity simulation.
 */

public class User implements Serializable {

    private String firstName;
    private String lastName;
    private Integer age;
    private String email;
    // Of course, the password should be encrypted, this is for demonstration purposes only
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("age=" + age)
                .add("email='" + email + "'")
                .add("password='" + password + "'")
                .toString();
    }

}
