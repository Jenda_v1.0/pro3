package cz.uhk.fim.pro3.cv3;

import cz.uhk.fim.pro3.cv3.user.User;
import cz.uhk.fim.pro3.cv3.user.UserStorage;
import cz.uhk.fim.pro3.cv3.user.UserSupport;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * User login example. <br/>
 * <i>Example of obtaining data from an HTTP request, their validation and response to missing mandatory values or
 * their incorrect syntax.</i>
 */

@WebServlet(name = "LogInServlet", value = "/log-in-servlet")
public class LogInServlet extends HttpServlet {

    private static void printBodyEnd(PrintWriter out) {
        out.println("<br/>");
        out.println("<a href=\"/page/exercises/cv3/cv3.jsp\">Cvičení 3</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

    private static void printUserTable(PrintWriter out, List<User> users) {
        out.println("<table border=\"1px solid black\" style=\"text-align: center; width: 80%\"><tr>");

        // Table header
        out.println("<th>Jméno</th>");
        out.println("<th>Příjmení</th>");
        out.println("<th>Věk</th>");
        out.println("<th>Email</th>");
        out.println("<th>Heslo</th>");
        out.println("</tr>");

        // Table contents
        users.forEach(user -> {
            out.println("<tr>");
            out.println("<td>" + user.getFirstName() + "</td>");
            out.println("<td>" + user.getLastName() + "</td>");
            out.println("<td>" + user.getAge() + "</td>");
            out.println("<td>" + user.getEmail() + "</td>");
            out.println("<td>" + user.getPassword() + "</td>");
            out.println("</tr>");
        });

        out.println("</table>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("Přihlášení");
        out.println("</title></head><body>");

        // Getting parameters from HTTP request
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        // Validation of input data:

        // Validation violation messages
        List<String> validationViolations = new ArrayList<>();

        // Check if the email is listed
        if (email == null || email.length() == 0) {
            validationViolations.add("Email není uveden.");
        } else if (!UserSupport.validateEmailSyntax(email)) {
            validationViolations.add("Email ('" + email + "') neodpovídá požadované syntaxi (například: 'homer.simpson@uhk.cz') nebo obsahuje nepovolené znaky.");
        }

        // Check if the password is listed
        if (password == null || password.length() == 0) {
            validationViolations.add("Heslo není uvedeno.");
        } else if (!UserSupport.validatePasswordSyntax(password)) {
            validationViolations.add("Heslo ('" + password + "') neodpovídá požadované syntaxi. Heslo musí obsahovat 5 až 50 znaků. Musí obsahovat alespoň jedno číslo, jedno velké písmeno, jedno malé písmeno a cizí znak (/*\\-+#%).");
        }

        // If there is at least one validation violation, we will display information about it
        if (!validationViolations.isEmpty()) {
            out.println("<h1>Přihlášení</h1>");
            out.println("<h2>Porušení validace</h2>");
            out.println("<ul>");
            validationViolations.forEach(validationViolation -> out.println("<li>" + validationViolation + "</li>"));
            out.println("</ul>");
            printBodyEnd(out);
            return;
        }

        Optional<User> optionalUser = UserStorage.findRegisteredUser(email);
        if (!optionalUser.isPresent()) {
            out.println("<h1>Přihlášení</h1>");
            out.println("<h2>Uživatel s emailovou adresou '<i>" + email + "'</i> nebyl nalezen.</h2>");
            printBodyEnd(out);
            return;
        }

        User user = optionalUser.get();
        // Check if the user has entered the correct password
        if (!user.getPassword().equals(password)) {
            out.println("<h1>Přihlášení</h1>");
            out.println("<h2>Nesprávné heslo.</h2>");
            printBodyEnd(out);
            return;
        }

        // User login (adding user information to the list of logged in users)
        UserStorage.logInUser(optionalUser.get());

        // Create a Session (if not) and insert user information into its attributes
        HttpSession session = request.getSession(true);
        session.setAttribute(UserSupport.LOGGED_IN_USER_ATTRIBUTE_NAME, user);

        // A page containing information about the logged in user and a list of all registered and logged in users
        out.println("<h1>Přihlášení</h1>");
        out.println("<h2>Vítejte <i>" + user.getFirstName() + " " + user.getLastName() + "</i></h2>");

        // Table of registered users
        out.println("<h2>Registrovaní uživatelé</h2>");
        printUserTable(out, UserStorage.getRegisteredUsers());

        // Table of logged in users
        out.println("<h2>Přihlášení uživatelé</h2>");
        printUserTable(out, UserStorage.getLoggedInUsers());

        printBodyEnd(out);
    }

}
