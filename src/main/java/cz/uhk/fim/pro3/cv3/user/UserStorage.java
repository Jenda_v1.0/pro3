package cz.uhk.fim.pro3.cv3.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Database simulation. <br/>
 * <i>Sample operations with database objects (user).</i>
 *
 * @author Jan Krunčík
 * @since 23.02.2022 18:18
 */

public class UserStorage {

    /**
     * Collection of all registered users. <br/>
     * <i>These users can 'log in'.</i>
     * <br/> Users are unique by email address.
     */
    private static final List<User> registeredUsers = new ArrayList<>();
    /**
     * Collection of logged in users. <br/>
     * <i>Users who 'log in' are added to this collection.</i>
     */
    private static final List<User> loggedInUsers = new ArrayList<>();

    /**
     * New user registration (adding it to the appropriate collection). <br/> It is checked whether the user with the
     * relevant email address does not already exist in the relevant collection.
     *
     * @param user
     *         the user to be registered
     *
     * @return true if the user was successfully registered (added to the appropriate collection), otherwise false
     */
    public static boolean registerUser(User user) {
        Optional<User> optionalUser = findRegisteredUser(user.getEmail());
        if (optionalUser.isPresent()) {
            return false;
        }

        return registeredUsers.add(user);
    }

    public static boolean logInUser(User user) {
        return loggedInUsers.add(user);
    }

    public static boolean logOutUser(String email) {
        return loggedInUsers.removeIf(loggedInUser -> loggedInUser.getEmail().equals(email));
    }

    public static Optional<User> findRegisteredUser(String email) {
        return findUser(email, registeredUsers);
    }

    private static Optional<User> findUser(String email, List<User> users) {
        return users.stream()
                    .filter(u -> u.getEmail().equals(email))
                    .findFirst();
    }

    public static List<User> getRegisteredUsers() {
        return registeredUsers;
    }

    public static List<User> getLoggedInUsers() {
        return loggedInUsers;
    }

}
