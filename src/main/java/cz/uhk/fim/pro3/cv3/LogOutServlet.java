package cz.uhk.fim.pro3.cv3;

import cz.uhk.fim.pro3.cv3.user.User;
import cz.uhk.fim.pro3.cv3.user.UserStorage;
import cz.uhk.fim.pro3.cv3.user.UserSupport;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Example of user logout.
 * <br/>
 * Example of obtaining data (identifier) of the logged in user from the Session attribute and its use for 'logging out' of the user.
 * <br/>
 * <i>In this example, logging off means removing the data of the logged in user from the Session and from the list of logged in users (database storage simulation).</i>
 * <br/>
 * <br/>
 * Method:
 * <br/>
 * - Get a Session
 * <br/>
 * - If a new one was created, it means that it did not exist yet, so the user could not be logged in
 * <br/>
 * - Get logged in user data from Session (from their attributes)
 * <br/>
 * - If no data was obtained, the user was not logged in
 * <br/>
 * - Transfer of acquired data from Session to {@link User} object and its logout (removal from collection of logged in users and from Session)
 */

@WebServlet(name = "LogOutServlet", value = "/log-out-servlet")
public class LogOutServlet extends HttpServlet {

    private static void printBodyEnd(PrintWriter out) {
        out.println("<br/>");
        out.println("<a href=\"/page/exercises/cv3/cv3.jsp\">Cvičení 3</a>");
        out.println("<br/>");
        out.println("<a href=\"index.html\">Domů</a>");

        // End of HTML page
        out.println("</body></html>");
        // Close stream object
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head><meta charset=\"UTF-8\"><title>");
        // Browser bookmark title
        out.println("Odhlášení");
        out.println("</title></head><body>");

        out.println("<h1>Odhlášení</h1>");

        // Getting a Session
        HttpSession session = request.getSession(true);
        if (session.isNew()) {
            out.println("<h2>Žádný uživatel nebyl přihlášen</h2>");
            printBodyEnd(out);
            return;
        }

        // Get the data of the logged in user
        Object objLoggedInUser = session.getAttribute(UserSupport.LOGGED_IN_USER_ATTRIBUTE_NAME);
        if (objLoggedInUser == null) {
            out.println("<h2>Žádný uživatel nebyl přihlášen</h2>");
            printBodyEnd(out);
            return;
        }

        // Conversion of user data to the appropriate Java object
        User loggedInUser = (User) objLoggedInUser;

        // User logout (removing a user from the list of logged in users).
        boolean userLogOut = UserStorage.logOutUser(loggedInUser.getEmail());
        if (!userLogOut) {
            out.println("<h2>Uživatele " + loggedInUser.getFirstName() + " " + loggedInUser.getLastName() + " se nepodařilo odhlásit</h2>");
            out.println("<div><i>(Nejspíše nebyl přihlášen)</i></div>");
            out.println("<div><i>" + loggedInUser + "</i></div>");
            printBodyEnd(out);
            return;
        }

        // Remove logged-in user information from Session:

        session.removeAttribute(UserSupport.LOGGED_IN_USER_ATTRIBUTE_NAME);
        // Or
        //session.setAttribute(UserSupport.LOGGED_IN_USER_ATTRIBUTE_NAME, null);

        // Calling session.invalidate() removes the session from the registry
        // Calling invalidate() will also remove all session attributes bound to the session
        session.invalidate();

        out.println("<h2><i>" + loggedInUser.getFirstName() + " " + loggedInUser.getLastName() + "</i> úspěšně odhlášen</h2>");
        out.println("<div><i>Údaje: " + loggedInUser + "</i></div>");

        printBodyEnd(out);
    }

}
