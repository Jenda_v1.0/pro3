package cz.uhk.fim.pro3.cv4;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Example of loading a file (/ image) and returning it to FE. <br/> Example of loading an image from the 'resources'
 * directory. An image (/ file) whose name will be passed in the HTTP request parameter will be loaded. <br/>
 * <i>Don't forget to catch exceptions.</i>
 */

@WebServlet(name = "ImageServlet", value = "/image-servlet")
public class ImageServlet extends HttpServlet {

    /**
     * The name of the attribute in the request that contains the name of the image to load (/ view).
     */
    private static final String IMAGE_PARAM_NAME = "imgName";
    /**
     * Path to the directory in the 'resources' directory containing the images (/ files) to load.
     */
    private static final String PATH_TO_IMAGE_DIR = "image";
    /**
     * The name of the default image to load if a parameter named image is not passed for loading.
     */
    private static final String DEFAULT_IMAGE = "mustang1.jpg";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");

        String imgName = request.getParameter(IMAGE_PARAM_NAME);
        if (imgName == null || imgName.replaceAll("\\s", "").length() == 0) {
            imgName = DEFAULT_IMAGE;
        }

        // Relative (to the 'resources' directory) path to the file (/ image) to load
        String filePath = PATH_TO_IMAGE_DIR + File.separator + imgName;
        InputStream is = getClass().getClassLoader().getResourceAsStream(filePath);

        BufferedInputStream bis = new BufferedInputStream(is);

        ServletOutputStream os = response.getOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(os);

        int ch;
        while ((ch = bis.read()) != -1) {
            bos.write(ch);
        }

        os.close();
        is.close();
        bis.close();
        bos.close();
    }

}
