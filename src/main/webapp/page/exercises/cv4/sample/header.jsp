<%@ page import="java.util.Enumeration" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Hlavička</title>
</head>
<body>
<h1>Hlavička</h1>
<h2>Atributy v hlavičce HTTP požadavku</h2>
<%
    Enumeration<String> headerNames = request.getHeaderNames();
    out.print("<ul>");
    while (headerNames.hasMoreElements()) {
        String headerName = headerNames.nextElement();
        String headerValue = request.getHeader(headerName);
        out.print("<li>" + headerName + " = " + headerValue + "</li>");
    }
    out.print("</ul>");
%>
<h2>Cookies</h2>
<%
    Cookie[] cookies = request.getCookies();
    out.print("<ul>");
    for (int i = 0; i < cookies.length; i++) {
        out.print("<li>" + cookies[i].getName() + " = " + cookies[i].getValue() + "</li>");
    }
    out.print("</ul>");
%>
<br/>
<br/>
<a href="/page/exercises/cv4/cv4.jsp">Cvičení 4</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
