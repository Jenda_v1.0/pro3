<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cykly</title>
</head>
<body>
<h1>Cykly</h1>
<h2>Java cyklus 1</h2>
<%-- Write for loop using Java syntax --%>
<%
    for (int i = 0; i < 3; i++) {
        out.print("<div>Knock</div>");
    }
    out.print("<div>Penny</div>");
%>
<br/>
<br/>
<h2>Java cyklus 2</h2>
<%
    for (int i = 1; i <= 5; i++) {
        out.print("<h" + i + ">Nadpis " + i + ". úrovně</h" + i + ">");
    }
%>
<br/>
<h2>JSTL cyklus</h2>
<%-- Write for a cycle using JSTL --%>
<c:forEach var="i" begin="1" end="5">
    <h${i}>Nadpis ${i}. úrovně</h${i}>
</c:forEach>
<br/>
<br/>
<a href="/page/exercises/cv4/cv4.jsp">Cvičení 4</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
