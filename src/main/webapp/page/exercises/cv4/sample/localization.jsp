<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- The following command manages the current language. If the language was supplied as request parameter (by language dropdown), then it will be set. Else if the language was already previously set in the session, then stick to it instead. Else use the user supplied locale in the request header. --%>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<%-- Sets the locale for resource bundle --%>
<fmt:setLocale value="${language}"/>

<%-- Name of the '.properties' file containing texts in a specific language (localization), if the (.properties) file is nested in a directory, separate the directory names (path) with a decimal point --%>
<%-- 'basename="localization"' finds the 'src/main/resources/localization_XX.properties' file --%>
<%-- 'basename="localization.localization"' finds the 'src/main/resources/localization/localization_XX.properties' file --%>
<fmt:setBundle basename="localization.localization"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><fmt:message key="localization.title"/></title>
</head>
<body>
<h1><fmt:message key="localization.h1"/></h1>
<div><fmt:message key="localization.selected_language"/>: '${language}'</div>
<br/>

<%-- Form for setting the language for the page, the selected language will be passed in the HTTP request parameter (to the current page) --%>
<form>
    <label for="language"><fmt:message key="localization.language_selection"/>: </label>
    <select id="language" name="language" onchange="submit()">
        <%-- The selected value will be added to the basename_(cs|en).properties file name --%>
        <option value="cs" ${language == 'cs' ? 'selected' : ''}>Čeština</option>
        <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
    </select>
</form>

<br/>

<div><fmt:message key="localization.div.text"/></div>

<ul>
    <li><fmt:message key="localization.ul.li.text1"/></li>
    <li><fmt:message key="localization.ul.li.text2"/></li>
    <li><fmt:message key="localization.ul.li.text3"/></li>
    <li><fmt:message key="localization.ul.li.text4"/></li>
</ul>

<br/>
<br/>
<a href="/page/exercises/cv4/cv4.jsp">Cvičení 4</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
