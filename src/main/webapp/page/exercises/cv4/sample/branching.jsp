<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Větvení</title>
</head>
<body>
<h1>Větvení</h1>
<c:forEach var="i" begin="${param['begin']}" end="${param['end']}">
    <div>${i} je
        <c:choose>
            <c:when test="${i < 5}">
                malé číslo
            </c:when>
            <c:when test="${i < 10}">
                střední číslo
            </c:when>
            <c:otherwise>
                velké číslo
            </c:otherwise>
        </c:choose>
    </div>
</c:forEach>
<br/>
<br/>
<a href="/page/exercises/cv4/cv4.jsp">Cvičení 4</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
