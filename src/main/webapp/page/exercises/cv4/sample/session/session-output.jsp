<%@ page import="java.util.Enumeration" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Session - Výstup</title>
</head>
<body>
<h1>Výpis údajů v Session</h1>
<%-- Entering data into the Session (if any data has been transmitted) --%>
<%
    // Get data from request when submitting form from 'src/main/webapp/page/exercises/cv4/sample/session/session.jsp'
    String sessionKey = request.getParameter("session-input-key");
    String sessionValue = request.getParameter("session-input-value");
    // If all values are specified, they will be inserted into the Session attributes
    if (sessionKey != null && !sessionKey.replaceAll("\\s", "").isEmpty() && sessionValue != null && !sessionValue.replaceAll("\\s", "").isEmpty()) {
        session.setAttribute(sessionKey, sessionValue);
    }
%>
<%-- Remove an attribute from a Session (if a removal key has been passed) --%>
<%
    String sessionRemoveKey = request.getParameter("session-remove-key");
    if (sessionRemoveKey != null && !sessionRemoveKey.replaceAll("\\s", "").isEmpty()) {
        session.removeAttribute(sessionRemoveKey);
    }
%>
<h2>Atributy v Session</h2>
<%
    Enumeration<String> attributeNames = session.getAttributeNames();
    if (!attributeNames.hasMoreElements()) {
        out.print("<div>V Session nejsou uvedeny žádné atributy.</div>");
    } else {
        out.print("<ul>");
        while (attributeNames.hasMoreElements()) {
            String attributeName = attributeNames.nextElement();
            Object attributeValue = session.getAttribute(attributeName);
            out.print("<li>" + attributeName + " = " + attributeValue + "</li>");
        }
        out.print("</ul>");
    }
%>
<br/>
<a href="/page/exercises/cv4/sample/session/session.jsp">Session</a>
<br/>
<br/>
<a href="/page/exercises/cv4/cv4.jsp">Cvičení 4</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
