<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Session</title>
</head>
<body>
<h1>Session</h1>
<div>Při potvrzení formuláře dojde k přesměrování na jinou stránku, ve které budou data zpracována (<em>údaje získány z
    HTTP požadavku a jejich zpracování v rámci příslušných operací se Session</em>).
</div>
<br/>
<a href="session-output.jsp">Údaje v Session</a>
<br/>
<h2>Vložit údaj do Session</h2>
<div>Vložení nové hodnoty do Session. Níže je třeba zadat klíč (<em>název</em>) atributu a jeho hodnotu, která bude
    vložena (<em>po potvrzení formuláře</em>) do Session.
</div>
<br/>
<form name="session-input" action="session-output.jsp" method="post">
    <label>
        Klíč: <input name="session-input-key" type="text">
    </label>
    <br/>
    <label>
        Hodnota: <input name="session-input-value" type="text">
    </label>
    <br/>
    <input type="submit" value="Uložit a zobrazit">
</form>
<br/>
<h2>Odebrat údaj ze Session</h2>
<div>Zadejte klíč (<em>název atributu</em>) v Session, který má být odebrán.</div>
<br/>
<form name="session-remove" action="session-output.jsp" method="post">
    <label>
        Klíč (pro odebrání): <input name="session-remove-key" type="text">
    </label>
    <br/>
    <input type="submit" value="Odebrat a zobrazit">
</form>
<br/>
<br/>
<a href="/page/exercises/cv4/cv4.jsp">Cvičení 4</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
