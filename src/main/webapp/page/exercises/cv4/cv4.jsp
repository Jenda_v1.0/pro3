<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cvičení 4</title>
</head>
<body>
<h1>Cvičení 4</h1>
<h2>Obrázky (<em>soubory</em>)</h2>
<ul>
    <li><a href="/image-servlet?imgName=mustang1.jpg">Mustang 1</a></li>
    <li><a href="/image-servlet?imgName=mustang2.jpg">Mustang 2</a></li>
    <li><a href="/image-servlet?imgName=mustang3.jpg">Mustang 3</a></li>
    <li><a href="/image-servlet?imgName=mustang4.jpg">Mustang 4</a></li>
</ul>
<h2>JSP a JSTL</h2>
<ul>
    <li><a href="/page/exercises/cv4/sample/loop.jsp">Cykly</a></li>
    <li><a href="/page/exercises/cv4/sample/header.jsp">Hlavička HTTP požadavku</a></li>
    <li><a href="/page/exercises/cv4/sample/branching.jsp?begin=1&end=15">Větvení</a></li>
    <li><a href="/page/exercises/cv4/sample/session/session.jsp">Session (<em>předávání hodnot mezi stránkami</em>)</a>
    </li>
    <li><a href="/page/exercises/cv4/sample/localization.jsp">Lokalizace</a></li>
</ul>
<br/>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
