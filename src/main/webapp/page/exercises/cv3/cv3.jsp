<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cvičení 3</title>
</head>
<body>
<h1>Cvičení 3</h1>
<h2>Registrace</h2>
<form name="registration" action="/registration-servlet" method="post">
    <label>
        Jméno: <input name="firstName" type="text">
    </label>
    <br/>
    <label>
        Příjmení: <input name="lastName" type="text">
    </label>
    <br/>
    <label>
        Věk: <input name="age" type="number">
    </label>
    <br/>
    <label>
        Email: <input name="email" type="email">
    </label>
    <br/>
    <label>
        Heslo: <input name="password1" type="password">
    </label>
    <br/>
    <label>
        Heslo znovu: <input name="password2" type="password">
    </label>
    <br/>
    <input type="submit" value="Registrovat">
</form>
<br/>
<h2>Přihlášení</h2>
<form name="log-in" action="/log-in-servlet" method="post">
    <label>
        Email: <input name="email" type="email">
    </label>
    <br/>
    <label>
        Heslo: <input name="password" type="password">
    </label>
    <br/>
    <input type="submit" value="Přihlásit">
</form>
<br/>
<h2>Odhlášení</h2>
<c:if test="${empty loggedInUser}">
    <div>Žádný uživatel není přihlášen</div>
</c:if>
<c:if test="${not empty loggedInUser}">
    <div>Přihlášený uživatel: ${loggedInUser.firstName} ${loggedInUser.lastName}</div>
    <div><em>${loggedInUser}</em></div>
    <br/>
    <a href="/page/exercises/cv3/home.jsp">Domovská stránka přihlášeného uživatele</a>
    <br/>
    <br/>
    <form name="log-out" action="/log-out-servlet" method="post">
        <input type="submit" value="Odhlásit">
    </form>
</c:if>
<br/>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
