<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Domov</title>
</head>
<body>
<h1>Domov</h1>
<h2>Domovská stránka přihlášeného uživatele</h2>
<div>Ahoj ${loggedInUser.firstName} ${loggedInUser.lastName}</div>
<ul>
    <li><a href="/index.html">Domů</a></li>
    <li><a href="/page/exercises/cv3/cv3.jsp">Cvičení 3</a></li>
</ul>
</body>
</html>
