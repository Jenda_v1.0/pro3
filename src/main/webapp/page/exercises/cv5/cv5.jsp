<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cvičení 5</title>
</head>
<body>
<h1>Cvičení 5</h1>
<ul>
    <li><a href="/person.dispatch">Osoba</a></li>
    <li><a href="/list-movie.dispatch">Filmy</a></li>
    <li><a href="/unknown.dispatch">Neznámá adresa v rámci centrálního <strong>dispečer</strong> servletu (<em>DispatcherServlet
        nezná tuto adresu</em>)</a></li>
    <li><a href="/unknown-page">Neznámá stránka (/ <em>adresa</em>) v rámci celé aplikace</a></li>
</ul>
<br/>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
