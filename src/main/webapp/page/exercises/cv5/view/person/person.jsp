<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Osoba</title>
</head>
<body>
<h1>Osoba</h1>
<%-- Call the 'toString()' method to print the data of the object --%>
<div>Objekt z <em>Model</em> vrstvy: ${person}</div>
<br/>
<%-- Access methods (getters) are used to obtain the values of the relevant object attributes, the names of which are composed automatically (get + attribute_name) --%>
<ul>
    <li>Databázové ID:
        <ul>
            <li><fmt:formatNumber value="${person.id}" type="NUMBER"/></li>
        </ul>
    </li>
    <li>Křestní jméno:
        <ul>
            <li>${person.firstName}</li>
        </ul>
    </li>
    <li>Příjmení:
        <ul>
            <li>${person.lastName}</li>
        </ul>
    </li>
    <li>Věk:
        <ul>
            <li>${person.age}</li>
        </ul>
    </li>
    <li>Datum narození (<em>bez formátování</em>:
        <ul>
            <li>${person.birthday}</li>
        </ul>
    </li>
    <li>Datum narození (<em>s formátování</em>:
        <ul>
            <li><fmt:formatDate value="${person.birthday}" pattern="dd.MM.yyyy"/></li>
        </ul>
    </li>
</ul>
<br/>
<br/>
<a href="/page/exercises/cv5/cv5.jsp">Cvičení 5</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
