<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Upravit</title>
</head>
<body>
<h1>Upravit</h1>
<c:if test="${not found}">
    <div><strong>Požadovaný film nebyl nalezen, není možné jej upravit.</strong></div>
</c:if>
<c:if test="${found}">
    <form name="create-movie" action="/update-movie.dispatch" method="post">
        <label>
            Název: <input name="title" type="text" value="${movie.title}">
        </label>
        <br/>
        <div style="max-width: 30em; border: 1px solid black">
            <div><label>Režisér</label></div>
            <input name="id" type="number" value="${movie.id}" hidden>
            <label>
                Křestní jméno: <input name="director.firstName" type="text" value="${movie.director.firstName}">
            </label>
            <br/>
            <label>
                Příjmení: <input name="director.lastName" type="text" value="${movie.director.lastName}">
            </label>
            <br/>
            <label>
                Věk: <input name="director.age" type="number" value="${movie.director.age}">
            </label>
            <br/>
            <label>
                Datum narození: <input name="director.birthday" type="date"
                                       value="<fmt:formatDate value="${movie.director.birthday}" pattern="yyyy-MM-dd"/>">
            </label>
        </div>
        <br/>
        <label>Žánr (<em>označit více možností pomocí 'Shift' nebo 'Ctrl'</em>):
            <select name="genres" multiple size="5">
                <option <c:if test="${fn:contains(movie.genres,'ACTION')}">selected</c:if>>ACTION</option>
                <option <c:if test="${fn:contains(movie.genres,'COMEDY')}">selected</c:if>>COMEDY</option>
                <option <c:if test="${fn:contains(movie.genres,'DRAMA')}">selected</c:if>>DRAMA</option>
                <option <c:if test="${fn:contains(movie.genres,'FANTASY')}">selected</c:if>>FANTASY</option>
                <option <c:if test="${fn:contains(movie.genres,'HORROR')}">selected</c:if>>HORROR</option>
                <option <c:if test="${fn:contains(movie.genres,'MYSTERY')}">selected</c:if>>MYSTERY</option>
                <option <c:if test="${fn:contains(movie.genres,'ROMANCE')}">selected</c:if>>ROMANCE</option>
                <option <c:if test="${fn:contains(movie.genres,'THRILLER')}">selected</c:if>>THRILLER</option>
                <option <c:if test="${fn:contains(movie.genres,'WESTERN')}">selected</c:if>>WESTERN</option>
                <option <c:if test="${fn:contains(movie.genres,'BIBLIOGRAPHY')}">selected</c:if>>BIBLIOGRAPHY</option>
                <option <c:if test="${fn:contains(movie.genres,'DOCUMENT')}">selected</c:if>>DOCUMENT</option>
                <option <c:if test="${fn:contains(movie.genres,'HISTORY')}">selected</c:if>>HISTORY</option>
            </select>
        </label>
        <br/>
        <label>
            Datum vydání: <input name="releaseDate" type="date"
                                 value="<fmt:formatDate value="${movie.releaseDate}" pattern="yyyy-MM-dd"/>">
        </label>
        <br/>
        <label>
            Popis: <input name="description" type="text" value="${movie.description}">
        </label>
        <br/>
        <input type="submit" value="Upravit">
    </form>
</c:if>
<br/>
<br/>
<a href="/list-movie.dispatch">Filmy</a>
<br/>
<a href="/page/exercises/cv5/cv5.jsp">Cvičení 5</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
