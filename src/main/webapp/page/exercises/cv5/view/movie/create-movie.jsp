<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Přidat film</title>
</head>
<body>
<h1>Přidat film</h1>
<form name="create-movie" action="/add-movie.dispatch" method="post">
    <label>
        Název: <input name="title" type="text">
    </label>
    <br/>
    <div style="max-width: 30em; border: 1px solid black">
        <div><label>Režisér</label></div>
        <label>
            Křestní jméno: <input name="director.firstName" type="text">
        </label>
        <br/>
        <label>
            Příjmení: <input name="director.lastName" type="text">
        </label>
        <br/>
        <label>
            Věk: <input name="director.age" type="number">
        </label>
        <br/>
        <label>
            Datum narození: <input name="director.birthday" type="date">
        </label>
    </div>
    <br/>
    <label>Žánr (<em>označit více možností pomocí 'Shift' nebo 'Ctrl'</em>):
        <select name="genres" multiple size="5">
            <option>ACTION</option>
            <option>COMEDY</option>
            <option>DRAMA</option>
            <option>FANTASY</option>
            <option>HORROR</option>
            <option>MYSTERY</option>
            <option>ROMANCE</option>
            <option>THRILLER</option>
            <option>WESTERN</option>
            <option>BIBLIOGRAPHY</option>
            <option>DOCUMENT</option>
            <option>HISTORY</option>
        </select>
    </label>
    <br/>
    <label>
        Datum vydání: <input name="releaseDate" type="date">
    </label>
    <br/>
    <label>
        Popis: <input name="description" type="text">
    </label>
    <br/>
    <input type="submit" value="Přidat">
</form>
<br/>
<br/>
<a href="/list-movie.dispatch">Filmy</a>
<br/>
<a href="/page/exercises/cv5/cv5.jsp">Cvičení 5</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
