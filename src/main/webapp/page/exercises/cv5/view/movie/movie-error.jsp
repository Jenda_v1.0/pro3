<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Chyba</title>
</head>
<body>
<h1>Chyba</h1>
<c:if test="${empty error}">
    <div>Při vykonávání příslušné operace došlo k chybě.</div>
</c:if>
<c:if test="${not empty error}">
    <div><strong>${error}</strong></div>
</c:if>
<br/>
<br/>
<a href="/list-movie.dispatch">Filmy</a>
<br/>
<a href="/page/exercises/cv5/cv5.jsp">Cvičení 5</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
