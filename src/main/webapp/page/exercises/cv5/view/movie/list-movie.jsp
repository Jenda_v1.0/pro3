<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Filmy</title>
</head>
<body>
<h1>Filmy</h1>
<a href="/page/exercises/cv5/view/movie/create-movie.jsp">Přidat film</a>
<br/>
<form name="filter-movies" action="/list-movie.dispatch" method="post">
    <label>
        Filtrovat dle názvu: <input name="title" type="text">
    </label>
    <input type="submit" value="Filtrovat">
</form>
<br/>
<table border="1px solid black" style="text-align: center; width: 80%">
    <tr>
        <th>ID</th>
        <th>Název</th>
        <th>Režisér</th>
        <th>Žánr</th>
        <th>Datum vydání</th>
        <th>Popis</th>
        <th>Upravit</th>
        <th>Detail</th>
        <th>Smazat</th>
    </tr>
    <c:forEach items="${movies}" var="movie">
        <tr>
            <td>${movie.id}</td>
            <td>${movie.title}</td>
            <td>${movie.director.firstName} ${movie.director.lastName}</td>
            <td>
                <c:forEach items="${movie.genres}" var="genre">
                    ${genre}<br/>
                </c:forEach>
            </td>
            <td><fmt:formatDate value="${movie.releaseDate}" pattern="dd.MM.yyyy"/></td>
            <td>${movie.description}</td>
            <td><a href="/get-movie-for-update-movie.dispatch?id=${movie.id}">Upravit</a></td>
            <td><a href="/detail-movie.dispatch?id=${movie.id}">Detail</a></td>
            <td><a href="/delete-movie.dispatch?id=${movie.id}">Smazat</a></td>
        </tr>
    </c:forEach>
</table>
<br/>
<br/>
<a href="/page/exercises/cv5/cv5.jsp">Cvičení 5</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
