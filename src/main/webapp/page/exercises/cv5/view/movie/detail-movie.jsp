<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Přehled filmu</title>
</head>
<body>
<h1>Přehled filmu</h1>
<c:if test="${not found}">
    <div><strong>Požadovaný film nebyl nalezen.</strong></div>
</c:if>
<c:if test="${found}">
    <ul>
        <li>ID
            <ul>
                <li><fmt:formatNumber value="${movie.id}" type="NUMBER"/></li>
            </ul>
        </li>
        <li>Název
            <ul>
                <li>${movie.title}</li>
            </ul>
        </li>
        <li>Režisér
            <ul>
                <li>ID
                    <ul>
                        <li><fmt:formatNumber value="${movie.director.id}" type="NUMBER"/></li>
                    </ul>
                </li>
                <li>Jméno
                    <ul>
                        <li>${movie.director.firstName} ${movie.director.lastName}</li>
                    </ul>
                </li>
                <li>Věk
                    <ul>
                        <li><fmt:formatNumber value="${movie.director.age}" type="NUMBER"/></li>
                    </ul>
                </li>
                <li>Narození
                    <ul>
                        <li><fmt:formatDate value="${movie.director.birthday}" pattern="dd.MM.yyyy"/></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>Žánr
            <ul>
                <c:forEach items="${movie.genres}" var="genre">
                    <li>${genre}</li>
                </c:forEach>
            </ul>
        </li>
        <li>Datum vydání
            <ul>
                <li><fmt:formatDate value="${movie.releaseDate}" pattern="dd.MM.yyyy"/></li>
            </ul>
        </li>
        <li>Popis
            <ul>
                <li>${movie.description}</li>
            </ul>
        </li>
    </ul>
</c:if>
<br/>
<br/>
<a href="/list-movie.dispatch">Filmy</a>
<br/>
<a href="/page/exercises/cv5/cv5.jsp">Cvičení 5</a>
<br/>
<a href="/index.html">Domů</a>
</body>
</html>
