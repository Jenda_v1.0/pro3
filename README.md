# PRO3

Příklady probírané na několika prvních cvičeních předmětu PRO3 vyučovaném na FIM UHK.

Projekt (_tato aplikace_) je zaměřen na úvod do webových aplikací v programovacím jazyce Java. Jsou zde obsaženy ukázky
komunikace FE <-> BE, práce s formuláři, předávání údajů, validace údajů, reagování na porušení validace vstupních
hodnot (_neuvedené povinné hodnoty, hodnoty v nesprávné syntaxi, práce s regulárními výrazy, …_), načítání souborů (_
obrázků_), Session (_přihlášení a odhlášení_), Java Servlet nebo návrhový vzor MVC. Dále základy HTML, JSP, JSTL nebo
lokalizace.

## Zprovoznění (_spuštění aplikace_)

### Aplikační server

* Stáhnout aplikační server, ve kterém bude aplikace spuštěna.
    * Například Tomcat (_verze 9 nebo nižší_)
        * Je k dispozici zde: https://gitlab.com/Jenda_v1.0/pro3/-/tree/Tomcat-9
        * Nebo na domovské stránce https://tomcat.apache.org/download-90.cgi
* Extrahovaný adresář vložte na Vámi zvolené umístění na Vašem (_používaném_) zařízení,
  například `C:\Application servers\apache-tomcat`.

### Aplikace

* Naklonovat (/ _stáhnout_) projekt z GitLab repositáře (https://gitlab.com/Jenda_v1.0/pro3/-/tree/2022/java-servlet)
* Otevřít ve Vámi zvoleném vývojovém prostředí (_následuje ukázka v prostředí IntelliJ IDEA_)
* Přidat aplikační server do vývojového prostředí
    * Otevřít nastavení (`Ctrl + Alt + S`)
    * **Build, Execution, Deployment** -> **Application Servers**
    * Přidat (_Vámi zvolený_) aplikační server
    * ![Application Server](setup/ApplicationServer.png)
* Otevřít okno _Run/Debug Configurations_
    * (`Alt + Shift + F10` -> následně stiskněte **nulu**)
* Přidat konfiguraci pro Vámi zvolený (_stažený_) aplikační server (_v našem případě Tomcat Server (Local)_), ve kterém
  budeme aplikaci spouštět
    * ![Add Tomcat](setup/AddTomcat.png)
    * Doplňte název konfigurace, například `PRO3 - Tomcat`
    * Zvolte výše přidaný aplikační server
    * V pravé spodní části tlačítko `Fix` -> `PRO3:war exploaded`
    * ![Run/Debug Configuration 1](setup/SetupRunDebugConfiguration1.png)
    * ![War Exploaded](setup/WarExploaded.png)
    * Záložka `Deployment` -> `Application context` -> Nastavit pouze na (_dopředné_) lomítko `/`
    * ![Run/Debug configuration 2](setup/SetupRunDebugConfiguration2.png)
    * Uložit změny a spustit
* Aplikace bude ve výchozím nastavení spuštěna na adrese http://localhost:8080/

## Struktura projektu

V aplikaci je možné se orientovat dle názvů balíčků odpovídajících jednotlivým cvičením, které obsahují (_mimo jiné_)
příklady probírané na jednotlivých cvičení ve škole.

Například balíček `src/main/java/cz/uhk/fim/pro3/cv1` obsahuje příklady z prvního cvičení.
Balíček `src/main/java/cz/uhk/fim/pro3/cv2` obsahuje příklady z druhého cvičení atd.

V těchto balíčcích jsou obsaženy soubory vztahující se k **Back-End** (_ové_) části webové aplikace.

V adresáři `src/main/webapp` jsou obsaženy veškeré soubory vztahující se k **Front-End** (_ové_) části webové aplikace.

Soubor `src/main/webapp/index.html` je výchozí (_domovská_) HTML stránka, která se zobrazí po spuštění aplikace.

Adresář `src/main/webapp/page/exercises` obsahuje podadresáře s názvy jednotlivých cvičení, které se **vztahují** k výše
uvedeném balíčkům pro Back-End.

Soubor `src/main/webapp/WEB-INF/web.xml` se nazývá _Deployment deskriptor_, který obsahuje nastavení, jak se mají URL
adresy mapovat na servlety, které URL adresy vyžadují ověření, nastavení platnosti (/ _expirace_) Session a další
informace.

Pro více informací viz https://cloud.google.com/appengine/docs/standard/java/config/webxml
